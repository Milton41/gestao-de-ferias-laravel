@extends('layout')

@section('content')
<div class="container">


<h1 class=""> Lista dos funcionarios!</h1>

<a href="{{route('lista.create')}}" class="btn btn-primary"> Adicionar</a>
<br>
<br>
<table class="table hover">
<tr>
<th>ID</th>
<th>Nome</th>
<th>Apelido </th>
<th> Endereco</th>
<th>Area de Job </th>
<th>Accao</th>
</tr>
@foreach($func as $funcionari)
<tr>
<td>{{$funcionari->id }}</td>
<td>{{$funcionari->nome }}</td>
<td>{{$funcionari->apelido}}</td>
<td>{{$funcionari->endereco }}</td>
<td>{{$funcionari->area}}</td>
<td><a href="" > <span class="bg-dark text-light"> Editar</span> </a></td>
<td><a href="" > <span class="bg-danger text-light"> Remover</span> </a></td>
</tr>
@endforeach


</table>


</div>


@endsection